(defpackage #:trivial-left-pad-test
  (:use #:cl #:prove #:trivial-left-pad))

(in-package #:trivial-left-pad-test)

(plan 7)

(subtest "Basic functionality tests"
  (is (left-pad "Hello world!" 15) "   Hello world!")
  (is (left-pad "Hello world!" 5) "Hello world!")
  (is (left-pad '|hello| 10) "     hello")
  (is (left-pad 42 42) "                                        42")
  (is (left-pad (cons 1 2) 8) " (1 . 2)"))

(subtest "Simple padding tests"
  (is (left-pad "foo" 5 nil) "  foo")
  (is (left-pad "foo" 5 #\Space) "  foo")
  (is (left-pad "foo" 9 #\f) "fffffffoo")
  (is (left-pad "foo" 5 #\_) "__foo")
  (is (left-pad "" 10 #\λ) "λλλλλλλλλλ")
  (is (left-pad #() 5 #\#) "###()"))

(subtest "String to char padding tests"
  (is (left-pad "foo" 9 "f") "fffffffoo")
  (is (left-pad "foo" 9 "f") "fffffffoo")
  (is (left-pad "foo" 5 "_") "__foo")
  (is (left-pad "foo" 5 " ") "  foo")
  (is (left-pad "" 10 "λ") "λλλλλλλλλλ")
  (is (left-pad '|λ| 10 "λ") "λλλλλλλλλλ"))

(subtest "Symbol to char padding tests"
  (is (left-pad '|foo| 9 'f) "FFFFFFfoo")
  (is (left-pad '|foo| 9 '|f|) "fffffffoo")
  (is (left-pad "foo" 5 '_) "__foo")
  (is (left-pad "foo" 5 '\ ) "  foo")
  (is (left-pad "" 10 '|λ|) "λλλλλλλλλλ")
  (is (left-pad '|| 10 '|λ|) "λλλλλλλλλλ")
  (is (left-pad '|λ| 10 '|λ|) "λλλλλλλλλλ"))

(subtest "Multiple character padding tests"
  (is (left-pad "foobar" 5 "!?") "foobar")
  (is (left-pad "bar" 6 '|foo|) "foobar")
  (is (left-pad "bar" 12 '|foo|) "foofoofoobar")
  (is (left-pad "bar" 10 '|foo|) "ofoofoobar")
  (is (left-pad "foo" 9 "-=#") "-=#-=#foo")
  (is (left-pad "foo" 11 "-=#") "=#-=#-=#foo"))

(subtest "Advanced usage test"
  ;; Constructing derived form right-pad from left-pad!
  (is (reverse (left-pad (reverse "foo") 10 "!")) "foo!!!!!!!"))

(subtest "Type error tests"
  (is-error (left-pad "foo" t) 'type-error)
  (is-error (left-pad "foo" -1) 'type-error)
  (is-error (left-pad "foo" 10000001) 'type-error)
  (is-error (left-pad "foo" 10 10) 'type-error))

(finalize)
