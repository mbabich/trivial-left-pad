(defpackage #:trivial-left-pad
  (:use #:cl)
  (:import-from #:alexandria #:string-designator)
  (:export #:left-pad))
