;;;; the LEFT-PAD function

;;;; Copyright (c) 2016, 2018 Michael Babich and Pascal J. Bourguignon
;;;;
;;;; Permission is hereby granted, free of charge, to any person obtaining
;;;; a copy of this software and associated documentation files (the
;;;; "Software"), to deal in the Software without restriction, including
;;;; without limitation the rights to use, copy, modify, merge, publish,
;;;; distribute, sublicense, and/or sell copies of the Software, and to
;;;; permit persons to whom the Software is furnished to do so, subject to
;;;; the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
;;;; LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
;;;; OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;;;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

(in-package #:trivial-left-pad)

;;; As reported on IRC, very long string lengths tend to exhaust heaps
;;; in Common Lisp implementations. 10,000,000 seems to be within a
;;; reasonable order of magnitude for a maximum value. Also, defining
;;; this maximum allows implementations to further optimize the code
;;; because it is usually a fixnum.
;;;
;;; It's impossible to guarantee that this is the best maximum string
;;; length for left-pad. Numbers larger than 10,000,000 might work on
;;; some implementations. You might also still get heap exhaustion at
;;; 10,000,000 on certain implementations or if you create a lot of
;;; strings with this length.
;;;
;;; If you use `left-pad' to produce a very long string, you probably
;;; shouldn't display the string as output in a SLIME emacs buffer.
(deftype reasonable-string-length ()
  `(integer 0 10000000))

;;; This uses a `FORMAT' string to accomplish the desired left pad
;;; behavior. Although ~A is common, these optional prefixes are
;;; not. v is used to read in the first two format-arguments as
;;; customized parameters of the prefix instead of hardcoding fixed
;;; behavior in or dynamically generating a format string. @ is used
;;; to pad on the left instead of the default right-pad behavior.
;;;
;;; ~mincol,colinc,minpad,padchar@A is the API for a left-padded A
;;; (Aesthetic) format. `left-pad' doesn't customize colinc or minpad
;;; so they are kept at their default values of 1 and 0. mincol
;;; (minimum columns) is equivalent to padding length.
;;;
;;; For more information, see section 22.3 of the Hyperspec,
;;; Formatted Output.
(declaim (inline %simple-left-pad))
(defun %simple-left-pad (string length padding)
  (declare (string string) (reasonable-string-length length) (character padding))
  (format nil "~v,,,v@A" length padding string))

(defun %advanced-left-pad (string length padding)
  (declare (string string) (reasonable-string-length length) (string padding))
  (multiple-value-bind (repeat remainder)
      (truncate (max 0 (- length (length string)))
                (length padding))
    (let ((*print-circle* nil))
      (format nil "~A~v{~A~:*~}~A"
              (subseq padding (- (length padding) remainder))
              repeat (list padding)
              string))))

;;; The function `left-pad' has been designed to be mostly equivalent
;;; to the node's left-pad. For more information, see README.md
(defun left-pad (string length &optional (padding #\Space))
  "Pads the left of string to be length long. It inserts padding in
front. If padding is nil, a space is inserted. If string is not a
string, it is converted to a string. The maximum allowed length is
10,000,000."
  ;; Nonsensical values should be caught here.
  (check-type length (reasonable-string-length))
  (check-type padding string-designator)
  (let ((string (typecase string
                  (string-designator (string string))
                  (t                 (format nil "~A" string)))))
    (cond
      ((null padding)
       (%simple-left-pad string length #\Space))
      ((characterp padding)
       (%simple-left-pad string length padding))
      ((or (and (stringp padding) (= 1 (length padding)))
           (and (symbolp padding) (= 1 (length (string padding)))))
       (%simple-left-pad string length (character padding)))
      (t (let* ((padding (string padding))
                (padding (if (zerop (length padding))
                             " "
                             padding)))
           (%advanced-left-pad string length padding))))))
